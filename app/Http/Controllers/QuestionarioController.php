<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QuestionarioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
         
        //checar se a forma que os parametros estao sendo usados estao corretos
        $nome = new App\User(['nome' => 'nome']);
        $pontos = new App\ (['pontosFeitos' => 'pontosFeitos']); //saber o endereço para buscar
        $aluno = App\Aluno::find(1); //talvez precise repetir para o segundo save
        $aluno->nome()->save($nome);
        $aluno->pontosFeitos()->save($pontos);
        $

        
        
        
        return view('questionario');
    }
}

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="info-users">
                <p>
                    <label>Email address</label>
                    <br><input type="email" name="" id="">
                </p>

                <p>
                    <label>Data</label>
                    <br><input type="date" name="" id="">
                </p>

                <p>
                    <label>Matrícula</label>
                    <br><input type="text" name="" id="">
                </p>

                <p>
                    <label>Nome Completo</label>
                    <br><input type="text" name="" id="">
                </p>

                <p>
                    <label>Curso</label>
                    <br><input type="text" name="" id="">
                </p>
            </div>
            <!-- Questionário -->
            <div>
                <p>1) De acordo com o regulamento da biblioteca é correto afirmar: Constituem usuários do Sistema de Bibliotecas da UNEB o corpo docente e o pessoal técnico-administrativo da Instituição, ativos, bem como o corpo discente, os estagiários nível médio e os prestadores de serviço. *</p>                
                <p><input type="radio" name="questao1" id="" value="Sim"> Sim</p>
                <p><input type="radio" name="questao1" id="" value="Não"> Não</p>
            </div>

            <div>
                <p>2) Segundo o regulamento é permitido: O usuário que contar com mais de um vínculo com a UNEB terá a homologação da sua inscrição em apenas uma das categorias, que por ele será escolhida. *</p>                
                <p><input type="radio" name="questao2" id="" value="Sim"> Sim</p>
                <p><input type="radio" name="questao2" id="" value="Não"> Não</p>
            </div>

            <div>
                <p>3) Usuário da graduação poderá ficar com quatro títulos por quinze dias. *</p>                
                <p><input type="radio" name="questao3" id="" value="Sim"> Sim</p>
                <p><input type="radio" name="questao3" id="" value="Não"> Não</p>
            </div>

            <div>
                <p>4) Posso adentrar no espaço portando meus pertences dentro da minha mochila/bolsa/sacola e/ou congêneres? *</p>                
                <p><input type="radio" name="questao4" id="" value="Sim"> Sim</p>
                <p><input type="radio" name="questao4" id="" value="Não"> Não</p>
            </div>

            <div>
                <p>5) Qual a alternativa correta? *</p>
                <p><input type="radio" name="questao5" id="" value="resp1"> Aluno de Graduação pega 04 livros por 15 dias</p>
                <p><input type="radio" name="questao5" id="" value="resp2"> Aluno de graduação pega 04 livros por 07 dias</p>
                <p><input type="radio" name="questao5" id="" value="resp3"> Aluno de Graduação pega 03 livros por 07 dias</p>
                <p><input type="radio" name="questao5" id="" value="resp4"> Aluno de graduação pega 05 livros por 15 dias</p>
                <p><input type="radio" name="questao5" id="" value="resp5"> Aluno de graduação pega 03 livros por 10 dias</p>
            </div>

            <div>
                <p>6) É dever do usuário da biblioteca (Múltipla escolha): *</p>
                <p><input type="checkbox" name="resp1" id=""> Evitar atitudes comportamentais e de comunicação que possam alterar a ordem ou a rotina da Biblioteca, evitando que lhe seja solicitado para ausentar-se do recinto ou, ainda, a depender da gravidade da falta cometida, ficar sujeito a outras penalidades previstas no Regimento Interno da Universidade e normas complementares sobre o assunto</p>
                <p><input type="checkbox" name="resp2" id=""> Manter o silêncio e atitudes compatíveis com o ambiente, sendo vedados o uso de telefone celular e o consumo de alimentos ou bebidas no recinto da Biblioteca</p>
                <p><input type="checkbox" name="resp3" id=""> utilizar as salas de leitura, exclusivamente, para atividades de estudo, pesquisa e recreação.</p>
                <p><input type="checkbox" name="resp4" id=""> Não comparecer à Biblioteca sempre que lhe for solicitado</p>
                <p><input type="checkbox" name="resp5" id=""> utilizar as salas de leitura, exclusivamente, para atividades de estudo e pesquisa.</p>
            </div>

            <div>
                <p>7) O aluno em situação irregular junto à Biblioteca está sujeito à suspensão de direitos na respectiva unidade de ensino tais como (Múltipla escolha): *</p>
                <p><input type="checkbox" name="resp1" id=""> Não poder entrar na biblioteca</p>
                <p><input type="checkbox" name="resp2" id=""> Solicitar transferências</p>
                <p><input type="checkbox" name="resp3" id=""> Receber diploma</p>
                <p><input type="checkbox" name="resp4" id=""> Ter direito a atestados e declarações</p>
                <p><input type="checkbox" name="resp5" id=""> Utilizar o empréstimo domiciliar</p>
                <p><input type="checkbox" name="resp6" id=""> Fazer pesquisas na base</p>
                <p><input type="checkbox" name="resp7" id=""> Renovar a matrícula</p>
            </div>

            <div>
                <p>8) Quem pode utilizar os serviços da Biblioteca Prof. Edivaldo Machado Boaventura? *</p>
                <p><input type="radio" name="questao8" id="" value="resp1"> Qualquer pessoa que tenha vínculo com a Instituição (Estudantes; Funcionários; Professores, Prestadores de Serviço e Estagiários).</p>
                <p><input type="radio" name="questao8" id="" value="resp2"> Apenas estudantes da UNEB</p>
                <p><input type="radio" name="questao8" id="" value="resp3"> Toda comunidade interna e externa da UNEB.</p>
            </div>

            <div>
                <p>9) Para ter acesso ao acervo o que é necessário? *</p>
                <p><input type="radio" name="questao9" id="" value="resp1"> Basta entrar na biblioteca com todos os seus pertences, inclusive bolsas.</p>
                <p><input type="radio" name="questao9" id="" value="resp2"> Nada. Não é possível ter acesso ao acervo.</p>
                <p><input type="radio" name="questao9" id="" value="resp3"> O usuário receberá um chaveiro numerado para o armário correspondente, para guarda dos pertences, tais como bolsas, pastas e/ou similares de uso pessoal.</p>
            </div>

            <div>
                <p>10) Quantos itens posso pegar emprestado e levar pra casa? *</p>
                <p><input type="radio" name="questao10" id="" value="resp1"> 03 livros; 02 folhetos; 02 multimeios (CDs e DVDs)</p>
                <p><input type="radio" name="questao10" id="" value="resp2"> 04 livros; 03 folhetos; 02 multimeios (CDs e DVDs)</p>
                <p><input type="radio" name="questao10" id="" value="resp3"> 05 livros; 02 folhetos; 01 multimeios (CDs e DVDs)</p>
            </div>

            <div>
                <p>11) Quantas vezes posso renovar o material bibliográfico? *</p>
                <p><input type="radio" name="questao11" id="" value="resp1"> 05 vezes seguidas, se não houver reservas</p>
                <p><input type="radio" name="questao11" id="" value="resp2"> 02 vezes seguidas, se não houver reservas</p>
                <p><input type="radio" name="questao11" id="" value="resp3"> 03 vezes seguidas, se não houver reservas</p>
            </div>

            <div>
                <p>12) Posso pedir para alguém pegar livros em meu nome? *</p>
                <p><input type="radio" name="questao12" id="" value="resp1"> Não! Sua senha é pessoal e intransferível sob qualquer pretexto.</p>
                <p><input type="radio" name="questao12" id="" value="resp2"> Sim! Sua senha pode ser compartilhada com todos.</p>
            </div>

            <div>
                <p>13) Posso pedir para alguém devolver os livros e meu nome? *</p>
                <p><input type="radio" name="questao13" id="" value="resp1"> Sim! A devolução independe da pessoa e a confirmação da devolução irá por e-mail.</p>
                <p><input type="radio" name="questao13" id="" value="resp2"> Não! É necessária a sua presença.</p>
            </div>

            <div>
                <p>14) Posso pegar emprestado algum material de outra Biblioteca da Uneb aqui em Salvador? *</p>
                <p><input type="radio" name="questao14" id="" value="resp1"> Sim! Acessando o site www.bemb.uneb.br, escolhendo a opção serviços; empréstimo interbibliotecário e preenchendo os dados do material solicitado e os seus dados</p>
                <p><input type="radio" name="questao14" id="" value="resp2"> Não! Você não pode pegar material de outra biblioteca.</p>
            </div>

            <div>
                <p>15) Posso devolver esse material em qualquer biblioteca da Uneb? *</p>
                <p><input type="radio" name="questao15" id="" value="resp1"> Não. A devolução deve ser feita na biblioteca, onde você pegou o material.</p>
                <p><input type="radio" name="questao15" id="" value="resp2"> Sim. Em bibliotecas que façam parte do SISB (Sistema de Biblioteca)/UNEB</p>
            </div>

            <div>
                <p>16) Caso eu atrase a entrega/devolução de qualquer material o que que acontece? *</p>
                <p><input type="radio" name="questao16" id="" value="resp1"> Você ficará suspenso dos serviços de empréstimos da Biblioteca.</p>
                <p><input type="radio" name="questao16" id="" value="resp2"> Você terá que pagar em reais, os dias de atraso.</p>
            </div>

            <div>
                <p>17) Se eu perder o livro ou parte(s) dele, danificá-lo com líquidos, marcas de caneta, rabiscos, marcador de texto, o que devo fazer? *</p>
                <p><input type="radio" name="questao17" id="" value="resp1"> As obras danificadas ou extraviadas pelo usuário serão por ele restauradas ou providenciadas a reposição por obra similar.</p>
                <p><input type="radio" name="questao17" id="" value="resp2"> Nada. O material é de responsabilidade da Instituição</p>
            </div>

            <div>
                <p>18) Se eu formar, abandonar ou trancar o curso, perco o direito ao empréstimo domiciliar? *</p>
                <p><input type="radio" name="questao18" id="" value="resp1"> Sim. O desligamento do usuário junto a Universidade, consiste na perda do direito ao empréstimo domiciliar.</p>
                <p><input type="radio" name="questao18" id="" value="resp2"> Não. Uma vez cadastrado(a), não será desligado(a)</p>
            </div>
        </div>
    </div>
</div>
@endsection